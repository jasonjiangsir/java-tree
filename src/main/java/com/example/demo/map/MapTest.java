package com.example.demo.map;

import cn.hutool.json.JSONUtil;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MapTest {
    public static void main(String[] args) {
        Map<Integer, List<Integer>> oldMap = new HashMap<Integer, List<Integer>>() {{
            put(710, new ArrayList<Integer>() {{
                add(706);
            }});
            put(711, new ArrayList<Integer>() {{
                add(702);
            }});
            put(712, new ArrayList<Integer>() {{
                add(709);
            }});
            put(713, new ArrayList<Integer>() {{
                add(704);
                add(708);
                add(709);
            }});
            put(714, new ArrayList<Integer>() {{
                add(705);
                add(704);
            }});
            put(715, new ArrayList<Integer>() {{
                add(707);
                add(705);
            }});
            put(716, new ArrayList<Integer>() {{
                add(708);
                add(703);
            }});
            put(717, new ArrayList<Integer>() {{
                add(708);
            }});
        }};

        Map<Integer, List<Integer>> newMap = new HashMap<>();
        oldMap.forEach((key, value) -> {
            value.forEach(id -> {
                List<Integer> idList = newMap.computeIfAbsent(id, k -> new ArrayList<>());
                idList.add(key);
            });
        });
        System.out.println(JSONUtil.toJsonStr(newMap));
        System.out.println("========");

        oldMap.forEach((key, value) -> {
            int i = 0;
            while (value.size() > 1 && i < value.size()) {
                Integer id = value.get(i);
                List<Integer> newIdList = newMap.get(id);
                if (newIdList.size() > 1) {
                    value.remove(id);
                    newMap.get(id).remove(key);
                } else {
                    i++;
                }
            }
        });
        System.out.println(JSONUtil.toJsonStr(oldMap));
        System.out.println("========");
        System.out.println(JSONUtil.toJsonStr(newMap));

    }
}
