package com.example.demo.MutiReturn;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.ImmutableTriple;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.lang3.tuple.Triple;

import java.util.ArrayList;
import java.util.List;

public class MutiReturn {

    static class User {
        private String id;
        private String name;
    }

    static class Role {
        private String id;
        private String name;
    }

    static class RoleGroup {
        private String id;
        private String name;
    }

    private static Pair<List<Role>, List<RoleGroup>> getRoleAndGroup() {
        List<Role> roleList = new ArrayList<>();
        List<RoleGroup> roleGroupList = new ArrayList<>();
        return ImmutablePair.of(roleList, roleGroupList);
    }

    private static Triple<User, List<Role>, List<RoleGroup>> getUserRoleAndGroup() {
        User user = new User();
        List<Role> roleList = new ArrayList<>();
        List<RoleGroup> roleGroupList = new ArrayList<>();
        return ImmutableTriple.of(user, roleList, roleGroupList);
    }

    public static void main(String[] args) {
        Pair<List<Role>, List<RoleGroup>> roleAndGroup = getRoleAndGroup();
        roleAndGroup.getKey();
        roleAndGroup.getLeft();
        roleAndGroup.getValue();
        roleAndGroup.getRight();


        Triple<User, List<Role>, List<RoleGroup>> userRoleAndGroup = getUserRoleAndGroup();
        userRoleAndGroup.getLeft();
        userRoleAndGroup.getMiddle();
        userRoleAndGroup.getRight();
    }

}
