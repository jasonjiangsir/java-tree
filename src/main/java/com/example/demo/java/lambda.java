package com.example.demo.java;

import cn.hutool.json.JSONUtil;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class lambda {

    static class Book {
        private Integer category;
        private String name;
        private BigDecimal price;

        public Book(Integer category, String name, BigDecimal price) {
            this.category = category;
            this.name = name;
            this.price = price;
        }

        public Integer getCategory() {
            return category;
        }

        public void setCategory(Integer category) {
            this.category = category;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public BigDecimal getPrice() {
            return price;
        }

        public void setPrice(BigDecimal price) {
            this.price = price;
        }
    }

    public static void main(String[] args) {
        List<Book> bookList = new ArrayList<Book>(){{
            add(new Book(1, "java", new BigDecimal("40")));
            add(new Book(1, "go", new BigDecimal("50")));
            add(new Book(1, "python", new BigDecimal("60")));
            add(new Book(1, "c", new BigDecimal("70")));
            add(new Book(2, "react", new BigDecimal("80")));
            add(new Book(2, "vue", new BigDecimal("90")));
            add(new Book(2, "ts", new BigDecimal("100")));
        }};

        Map<Boolean, List<Book>> bookMap = bookList.stream().collect(Collectors.partitioningBy(item -> item.getCategory() == 1));
        System.out.println(JSONUtil.toJsonStr(bookMap));
        Map<Integer, List<Book>> bookeMap1 = bookList.stream().collect(Collectors.groupingBy(Book::getCategory));
        System.out.println(JSONUtil.toJsonStr(bookeMap1));
        BigDecimal sum = bookList.stream().map(Book::getPrice).reduce(BigDecimal.ZERO, BigDecimal::add);

    }

}
